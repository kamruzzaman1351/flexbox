{/* <div class="box">
    <h2><i class="fas fa-mobile"></i>Responsive Design</h2>
    <p>
    Flexbox makes building a website layout (and making it responsive!)
    much, much easier
    </p>
</div> */}
// Creating the html code using js
const container = document.querySelector(".boxes .container");
const box = document.createElement("div");
const heading = document.createElement("h2");
const headText = document.createTextNode("Responsive Design");
const text = document.createElement("p");
text.appendChild(document.createTextNode(`Flexbox makes building a website layout and making it responsive!
much, much easier`));
heading.setAttribute("href", "https://google.com");

heading.innerHTML = `<i class="fas fa-mobile"></i>`;
heading.appendChild(headText);
box.className = "box";
box.appendChild(heading);
box.appendChild(text);
container.appendChild(box);

// output
console.log(box);
// console.log(heading);